/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ht.inheritance1;

/**
 *
 * @author ACER
 */
public class Cat extends Animal {
    public Cat (String name , String color) {
        super(name , color , 4);
        System.out.println("Cat Created");
    }
        @Override
    public void walk() {
        super.walk();
        System.out.println("Cat : " + this.name + " walk with " + this.numLegs + " legs.");
    }
    
    @Override
    public void speak() {
        super.speak();
        System.out.println("Cat : " + this.name + " speak > Meow Meow !!!");
    }
 }
    
