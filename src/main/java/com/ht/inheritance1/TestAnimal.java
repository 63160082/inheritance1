/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ht.inheritance1;

/**
 *
 * @author ACER
 */
public class TestAnimal {
    public static void main(String[] args) {
        Animal animal = new Animal("Ani", "White" , 0);
        animal.speak();
        animal.walk();
        
        System.out.println("------------------------------------");
        Dog dang = new Dog ("Dang" , "black&white");
        dang.speak();
        dang.walk();
        
        System.out.println("------------------------------------");
        Cat zero = new Cat("Zero" , "Orange");
        zero.speak();
        zero.walk();

        System.out.println("------------------------------------");
        Duck zom = new Duck("Zom" , "Orange");
        zom.speak();
        zom.walk();
        zom.fly();
        
        System.out.println("------------------------------------");
        Duck gabgab = new Duck("GabGab" , "Orange");
        gabgab.speak();
        gabgab.walk();
        gabgab.fly();
        
        System.out.println("------------------------------------");
        Dog to = new Dog ("To" , "black&white");
        to.speak();
        to.walk();

        System.out.println("------------------------------------");
        Dog bat = new Dog ("Bat" , "black&white");
        bat.speak();
        bat.walk();

        System.out.println("------------------------------------");
        Dog mome = new Dog ("Mome" , "black&white");
        mome.speak();
        mome.walk();        
                      
        System.out.println("Zom is Animal : " + (zom instanceof Animal));
        System.out.println("Zom is Animal : " + (zom instanceof Duck));
        System.out.println("Zom is Animal : " + (zom instanceof Object));   
        System.out.println("Animal is Dog : " + (animal instanceof Dog));
        System.out.println("Animal is Animal : " + (animal instanceof Animal));  
        System.out.println("GabGab is Animal :" + (gabgab instanceof Animal));
        System.out.println("GabGab is Duck :" + (gabgab instanceof Duck));
        
        Animal ani1 = null;
        Animal ani2 = null;
        ani1 = zom;
        ani2 = zero;
        
        System.out.println("Ani1 : Zom is Duck : " + (ani1 instanceof Duck));
        
        Animal [] animals = {dang , zero , zom};
            for(int i = 0 ; i < animals.length ; i++) {
                animals[i].walk();
                animals[i].speak();
            if(animals[i] instanceof Duck) {
                Duck duck = (Duck)animals[i];
                duck.fly();
            }   
            }
        
    }
        
}
