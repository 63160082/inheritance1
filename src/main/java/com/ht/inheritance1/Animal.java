/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ht.inheritance1;

/**
 *
 * @author ACER
 */
public class Animal {
    protected String name;
    protected int numLegs = 0;
    protected String color;
    
    public Animal(String name , String color , int numLegs) {
        System.out.println("Animal Created");
        this.name = name;
        this.numLegs = numLegs;
        this.color = color;
    }
    
    public void walk() {
        System.out.println("Animal Walk");
    }
    
    public void speak() {
        System.out.println("Animal Speak");
        System.out.println("Name : " + this.name + " ,Color : " + this.color + " ,Number of Legs : " + this.numLegs);
    }

    public String getName() {
        return name;
    }

    public int getNumLegs() {
        return numLegs;
    }

    public String getColor() {
        return color;
    }
    
    
}
