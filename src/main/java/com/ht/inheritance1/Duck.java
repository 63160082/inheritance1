/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ht.inheritance1;

/**
 *
 * @author ACER
 */
public class Duck extends Animal {
    private int wings;
    public Duck (String name , String color) {
    super(name , color , 2);
    System.out.println("Duck Crated");
    }
    
        @Override
    public void walk() {
        super.walk();
        System.out.println("Duck : " + this.name + " walk with " + this.numLegs + " legs.");
    }
    
    @Override
    public void speak() {
        super.speak();
        System.out.println("Duck : " + this.name + " speak > Gab Gab !!!");
    }
    
    public void fly() {
        System.out.println("Duck : " + this.name + " fly !!!");
    }
            
}
